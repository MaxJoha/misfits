#include <stdio.h>

int main()
{
    int first, second, add, subtract, multiply;
    float divide;

    printf("Enter two integers\n");
    scanf("%d%d", &first, &second);

    add = first + second;
    subtract = first - second;
    divide = first / (float)second;
    multiply = first * second;

    printf("Sum = %d\n", add);
    printf("difference = %d\n", subtract);
    printf("division = %.2f\n", divide);
    printf("Multiplication = %d\n", multiply);

    return 0;
}